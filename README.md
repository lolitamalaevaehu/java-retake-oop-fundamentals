# Task 1. Methods of output, finding the sum and the largest value (Checking the task by a mentor)

## Solve the task:

Create a `Test1` class with two variables. Add a method for displaying and methods for changing these variables. Add a method that finds the sum of the values of these variables, and a method that finds the largest value of these two variables.

_Place the solved task in an open Git repository._


# Task 2. Set- and get- methods (Checking the task by a mentor)

## Solve the task:

Create a `Test2` class with two variables. Add a constructor with input parameters. Add a constructor that initializes the default class members. Add `set`- and `get`- methods for the instance fields of the class.

_Place the solved task in an open Git repository._

# Task 3. Triangle (Checking the task by a mentor)

## Solve the task:

Describe the class representing the triangle. A triangle is described by three sides. Provide methods for creating objects, calculating the area, perimeter.

_Place the solved task in an open Git repository._

# Task 4. Decimal counter (Checking the task by a mentor)

## Solve the task:

Describe a class implementing a decimal counter that can increase or decrease its value by one in a given range. Provide initialization of the counter with default values and arbitrary values. The counter has methods for increasing and decreasing the state, and a method for getting its current state. Write code that demonstrates all the features of the class.

_Place the solved task in an open Git repository._