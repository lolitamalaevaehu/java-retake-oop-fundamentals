package part2.test1;

public class Main {
    public static void main(String[] args) {
        // Array of ten students
        Student[] students = new Student[10];

        // Assuming the academicPerformance array has 5 elements
        students[0] = new Student("Ivanov I.I.", "1A", new int[] {9, 9, 10, 10, 9});
        for (Student student : students) {
            if (student != null && student.isTopStudent()) {
                System.out.println(student.getSurnameAndInitials() + " from group " + student.getGroupNumber());
            }
        }
    }
}

