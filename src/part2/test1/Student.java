package part2.test1;

public class Student {
    private String surnameAndInitials;
    private String groupNumber;
    private int[] academicPerformance;

    // Constructor
    public Student(String surnameAndInitials, String groupNumber, int[] academicPerformance) {
        this.surnameAndInitials = surnameAndInitials;
        this.groupNumber = groupNumber;
        this.academicPerformance = academicPerformance;
    }

    // Getter for surname and initials
    public String getSurnameAndInitials() {
        return surnameAndInitials;
    }

    // Getter for group number
    public String getGroupNumber() {
        return groupNumber;
    }

    // Method to determine if the student has only 9s and 10s
    public boolean isTopStudent() {
        for (int grade : academicPerformance) {
            if (grade < 9) {
                return false;
            }
        }
        return true;
    }
}

