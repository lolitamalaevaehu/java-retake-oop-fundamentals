package part2.test4;

import java.util.ArrayList;
import java.util.List;

public class Library {
    private List<Book> books;

    // Constructor
    public Library() {
        this.books = new ArrayList<>();
    }

    // Method to add a book
    public void addBook(Book book) {
        books.add(book);
    }

    // a) Find books by a given author
    public List<Book> findBooksByAuthor(String author) {
        List<Book> foundBooks = new ArrayList<>();
        for (Book book : books) {
            if (book.getAuthor().equalsIgnoreCase(author)) {
                foundBooks.add(book);
            }
        }
        return foundBooks;
    }

    // b) Find books published by a given publisher
    public List<Book> findBooksByPublisher(String publisher) {
        List<Book> foundBooks = new ArrayList<>();
        for (Book book : books) {
            if (book.getPublisher().equalsIgnoreCase(publisher)) {
                foundBooks.add(book);
            }
        }
        return foundBooks;
    }

    // c) Find books released after a given year
    public List<Book> findBooksAfterYear(int year) {
        List<Book> foundBooks = new ArrayList<>();
        for (Book book : books) {
            if (book.getYearOfPublication() > year) {
                foundBooks.add(book);
            }
        }
        return foundBooks;
    }
}

