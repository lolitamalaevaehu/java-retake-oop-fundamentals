package part2.test4;

public class Main {
    public static void main(String[] args) {
        Library library = new Library();
        library.addBook(new Book(1, "Book1", "Author1", "Publisher1", 2000, 300, 29.99, "Hardcover"));
        library.addBook(new Book(2, "Book2", "Author2", "Publisher2", 2005, 250, 19.99, "Paperback"));
        // Add more books as needed

        // Display books by a specific author
        System.out.println("Books by Author1:");
        for (Book book : library.findBooksByAuthor("Author1")) {
            System.out.println(book);
        }

        // Display books from a specific publisher
        System.out.println("\nBooks published by Publisher2:");
        for (Book book : library.findBooksByPublisher("Publisher2")) {
            System.out.println(book);
        }

        // Display books released after a specific year
        System.out.println("\nBooks released after 2003:");
        for (Book book : library.findBooksAfterYear(2003)) {
            System.out.println(book);
        }
    }
}

