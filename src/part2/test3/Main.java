package part2.test3;

public class Main {
    public static void main(String[] args) {
        Customer[] customers = {
                new Customer(1, "Smith", "John", "Edward", "1234 Main St", 5555666677778888L, 123456789L),
                new Customer(2, "Doe", "Jane", "Ann", "5678 Park Ave", 4444555566667777L, 987654321L),
        };

        CustomerDatabase database = new CustomerDatabase(customers);

        // Print all customers alphabetically
        System.out.println("Customers in alphabetical order:");
        database.printCustomersAlphabetically();

        // Print customers whose credit card number is in the specified range
        long minCreditCardNumber = 4000000000000000L;
        long maxCreditCardNumber = 5000000000000000L;
        System.out.println("\nCustomers with a credit card number in the specified interval:");
        database.printCustomersByCreditCardRange(minCreditCardNumber, maxCreditCardNumber);
    }
}

