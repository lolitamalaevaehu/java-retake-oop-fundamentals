package part2.test3;

import java.util.Arrays;

public class CustomerDatabase {
    private Customer[] customers;

    // Constructor
    public CustomerDatabase(Customer[] customers) {
        this.customers = customers;
    }

    // Method to sort customers alphabetically
    public void sortCustomersAlphabetically() {
        Arrays.sort(customers, (c1, c2) -> c1.getSurname().compareToIgnoreCase(c2.getSurname()));
    }

    // Method to print customers alphabetically
    public void printCustomersAlphabetically() {
        sortCustomersAlphabetically();
        for (Customer customer : customers) {
            System.out.println(customer);
        }
    }

    // Method to print customers with a credit card number in the specified interval
    public void printCustomersByCreditCardRange(long min, long max) {
        for (Customer customer : customers) {
            if (customer.getCreditCardNumber() >= min && customer.getCreditCardNumber() <= max) {
                System.out.println(customer);
            }
        }
    }
}

