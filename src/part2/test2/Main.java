package part2.test2;

import java.time.LocalTime;
import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Train[] trains = {
                new Train("Los Angeles", 102, LocalTime.of(10, 45)),
                new Train("San Francisco", 101, LocalTime.of(14, 30)),
                new Train("New York", 104, LocalTime.of(9, 00)),
                new Train("Chicago", 100, LocalTime.of(16, 15)),
                new Train("San Francisco", 103, LocalTime.of(13, 45))
        };

        // Sort trains by number
        Arrays.sort(trains, Train.byTrainNumber);
        System.out.println("Trains sorted by train number:");
        for (Train train : trains) {
            System.out.println(train);
        }

        // User input for train number
        Scanner scanner = new Scanner(System.in);
        System.out.println("\nEnter train number to get information:");
        int number = scanner.nextInt();

        // Display information for entered train number
        boolean found = false;
        for (Train train : trains) {
            if (train.getTrainNumber() == number) {
                System.out.println("Information for train number " + number + ":");
                System.out.println(train);
                found = true;
                break;
            }
        }

        if (!found) {
            System.out.println("No train found with number: " + number);
        }

        // Sort trains by destination, then by departure time
        Arrays.sort(trains, Train.byDestinationThenTime);
        System.out.println("\nTrains sorted by destination and then by departure time:");
        for (Train train : trains) {
            System.out.println(train);
        }

        scanner.close();
    }
}

