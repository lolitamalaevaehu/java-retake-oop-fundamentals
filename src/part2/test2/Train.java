package part2.test2;

import java.time.LocalTime;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;

public class Train {
    private String destinationName;
    private int trainNumber;
    private LocalTime departureTime; // Use LocalTime for time to utilize built-in comparison

    // Constructor
    public Train(String destinationName, int trainNumber, LocalTime departureTime) {
        this.destinationName = destinationName;
        this.trainNumber = trainNumber;
        this.departureTime = departureTime;
    }

    // Getters
    public String getDestinationName() {
        return destinationName;
    }

    public int getTrainNumber() {
        return trainNumber;
    }

    public LocalTime getDepartureTime() {
        return departureTime;
    }

    // A comparator for sorting by train number
    public static Comparator<Train> byTrainNumber = Comparator.comparingInt(Train::getTrainNumber);

    // A comparator for sorting by destination, then by departure time
    public static Comparator<Train> byDestinationThenTime = Comparator
            .comparing(Train::getDestinationName)
            .thenComparing(Train::getDepartureTime);

    // Method to display train information
    @Override
    public String toString() {
        return "Train Number: " + trainNumber +
                ", Destination: " + destinationName +
                ", Departure Time: " + departureTime;
    }
}

