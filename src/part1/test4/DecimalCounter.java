package part1.test4;

public class DecimalCounter {
    private int currentValue;
    private final int minValue;
    private final int maxValue;

    // Constructor with default values
    public DecimalCounter() {
        this(0, 0, 100); // Default range 0 to 100
    }

    // Constructor with custom range and initial value
    public DecimalCounter(int initialValue, int minValue, int maxValue) {
        this.minValue = minValue;
        this.maxValue = maxValue;
        setCurrentValue(initialValue);
    }

    public void increment() {
        if (currentValue < maxValue) {
            currentValue++;
        }
    }

    public void decrement() {
        if (currentValue > minValue) {
            currentValue--;
        }
    }

    public int getCurrentValue() {
        return currentValue;
    }

    private void setCurrentValue(int value) {
        if (value >= minValue && value <= maxValue) {
            currentValue = value;
        } else {
            currentValue = minValue;
        }
    }
}

