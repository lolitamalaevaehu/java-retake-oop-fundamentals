package part1.test4;

public class Main {
    public static void main(String[] args) {
        // Using the default constructor
        DecimalCounter counterDefault = new DecimalCounter();
        System.out.println("Default Counter Initial Value: " + counterDefault.getCurrentValue());
        counterDefault.increment();
        System.out.println("Default Counter After Increment: " + counterDefault.getCurrentValue());
        counterDefault.decrement();
        System.out.println("Default Counter After Decrement: " + counterDefault.getCurrentValue());

        // Using the constructor with custom values
        DecimalCounter counterCustom = new DecimalCounter(5, 0, 10);
        System.out.println("\nCustom Counter Initial Value: " + counterCustom.getCurrentValue());
        counterCustom.increment();
        System.out.println("Custom Counter After Increment: " + counterCustom.getCurrentValue());
        counterCustom.decrement();
        counterCustom.decrement();
        System.out.println("Custom Counter After Two Decrements: " + counterCustom.getCurrentValue());
    }
}

