package part1.test2;

public class Main {
    public static void main(String[] args) {
        // Create an instance of Test2 using the constructor with parameters
        Test2 test1 = new Test2(10, 20);
        System.out.println("Test1 - Variable1: " + test1.getVariable1() + ", Variable2: " + test1.getVariable2());

        // Create an instance of Test2 using the default constructor
        Test2 test2 = new Test2();
        System.out.println("Test2 (Default) - Variable1: " + test2.getVariable1() + ", Variable2: " + test2.getVariable2());

        //Setting new values for test2
        test2.setVariable1(30);
        test2.setVariable2(40);
        System.out.println("Test2 (After Setting) - Variable1: " + test2.getVariable1() + ", Variable2: " + test2.getVariable2());
    }
}

