package part1.test2;

public class Test2 {
    private int variable1;
    private int variable2;

    // Constructor with input parameters
    public Test2(int variable1, int variable2) {
        this.variable1 = variable1;
        this.variable2 = variable2;
    }

    // Default constructor
    public Test2() {
        this.variable1 = 0; // Default value for variable1
        this.variable2 = 0; // Default value for variable2
    }

    // Set method for variable1
    public void setVariable1(int variable1) {
        this.variable1 = variable1;
    }

    // Get method for variable1
    public int getVariable1() {
        return this.variable1;
    }

    // Set method for variable2
    public void setVariable2(int variable2) {
        this.variable2 = variable2;
    }

    // Get method for variable2
    public int getVariable2() {
        return this.variable2;
    }
}
