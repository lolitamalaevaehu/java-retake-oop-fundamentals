package part1.test3;

public class Main {
    public static void main(String[] args) {
        Triangle triangle = new Triangle(3, 4, 5);

        double perimeter = triangle.getPerimeter();
        double area = triangle.getArea();

        System.out.println("Perimeter of the triangle: " + perimeter);
        System.out.println("Area of the triangle: " + area);
    }
}

