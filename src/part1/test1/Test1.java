package part1.test1;

public class Test1 {
    private int variable1;
    private int variable2;

    // Constructor to initialize the variables
    public Test1(int variable1, int variable2) {
        this.variable1 = variable1;
        this.variable2 = variable2;
    }

    // Method to display the variables
    public void displayVariables() {
        System.out.println("Variable1: " + variable1 + ", Variable2: " + variable2);
    }

    // Method to change variable1
    public void setVariable1(int variable1) {
        this.variable1 = variable1;
    }

    // Method to change variable2
    public void setVariable2(int variable2) {
        this.variable2 = variable2;
    }

    // Method to calculate the sum of the variables
    public int sum() {
        return variable1 + variable2;
    }

    // Method to find the maximum of the two variables
    public int max() {
        return Math.max(variable1, variable2);
    }
}