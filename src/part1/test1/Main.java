package part1.test1;

public class Main {
    public static void main(String[] args) {
        Test1 test = new Test1(10, 20);
        test.displayVariables(); // Displays the initial values

        test.setVariable1(15); // Changes variable1 to 15
        test.setVariable2(25); // Changes variable2 to 25
        test.displayVariables(); // Displays the updated values

        System.out.println("Sum: " + test.sum()); // Displays the sum
        System.out.println("Max: " + test.max()); // Displays the maximum value
    }
}
